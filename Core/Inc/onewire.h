/*
 * onewire.h
 *
 *  Created on: Nov 28, 2021
 *      Author: Marcin
 */

#ifndef INC_ONEWIRE_H_
#define INC_ONEWIRE_H_

#define DS18B20_GPIO GPIOA
#define DS18B20_PIN_NR GPIO_PIN_2

#include <stdint.h>

void delay_us(uint16_t time_us);
uint8_t wire_Reset();
void write_zero();
void write_one();
uint8_t read_bit();


void write_byte(uint8_t byte);
uint8_t read_byte();

uint8_t byte_crc(uint8_t crc, uint8_t byte);
uint8_t wire_crc(const uint8_t* data, int len);



#endif /* INC_ONEWIRE_H_ */
