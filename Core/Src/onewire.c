/*
 * onewire.c
 *
 *  Created on: Nov 28, 2021
 *      Author: Marcin
 */

#include "tim.h"
#include "onewire.h"


void delay_us(uint16_t time_us)
{
	HAL_TIM_Base_Start(&htim4);
	TIM4->CNT = 0;
	while(TIM4->CNT < time_us);
	HAL_TIM_Base_Stop(&htim4);
}

uint8_t wire_Reset()
{
	uint8_t response;
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_RESET);
	delay_us(480);
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_SET);
	delay_us(70);
	response = (HAL_GPIO_ReadPin(DS18B20_GPIO, DS18B20_PIN_NR));
	delay_us(410);
	return response;
}

void write_zero()
{
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_RESET);
	delay_us(60);
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_SET);
	delay_us(10);
}

void write_one()
{
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_RESET);
	delay_us(6);		//moze zmienic na 6
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_SET);
	delay_us(64);
}

uint8_t read_bit()
{
	uint8_t read_bit;
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_RESET);
	delay_us(6);
	HAL_GPIO_WritePin(DS18B20_GPIO, DS18B20_PIN_NR, GPIO_PIN_SET);
	delay_us(9);
	read_bit = HAL_GPIO_ReadPin(DS18B20_GPIO, DS18B20_PIN_NR);
	delay_us(55);
	return read_bit;
}

void write_byte(uint8_t byte)
{
	for(uint8_t i = 0; i < 8; i++)
	{
		if(byte & 0x01)
		{
			write_one();
		}
		else
		{
			write_zero();
		}
		byte >>= 1;			//byte = byte >> 1
	}
}

uint8_t read_byte()
{
	uint8_t our_byte = 0;
	for(uint8_t i = 0; i < 8; i++)
	{
		our_byte >>= 1;
		if(read_bit())
		{
			our_byte |= 0x80;
		}
	}
	return our_byte;
}

uint8_t byte_crc(uint8_t crc, uint8_t byte)
{
  int i;
  for (i = 0; i < 8; i++) {
    uint8_t b = crc ^ byte;
    crc >>= 1;
    if (b & 0x01)
      crc ^= 0x8c;
    byte >>= 1;
  }
  return crc;
}

uint8_t wire_crc(const uint8_t* data, int len)
{
  int i;
    uint8_t crc = 0;

    for (i = 0; i < len; i++)
      crc = byte_crc(crc, data[i]);

    return crc;
}
